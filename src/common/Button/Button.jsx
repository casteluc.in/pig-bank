import React from 'react'
import { Link } from 'react-router-dom'

import './Button.css'

function Button({label, path, classes}) {
    return (
        <Link to={path} className={`button ${classes && classes}`}>
            {label}                            
        </Link>
    )
}

export default Button  
