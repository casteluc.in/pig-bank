import React from 'react'

import './Footer.css'
function Footer({fixed}) {
    return (
        <footer className={fixed && "fixed"}>
            <p>feito com amor</p>
        </footer>
    )
}

export default Footer
