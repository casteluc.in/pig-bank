import React, { useState } from 'react'

import Button from '../Button/Button'

import './Header.css'

function Menu({home, coins}) {
    const [status, setStatus] = useState("")

    const openMenu = e => {
        if (status === "") {
            setStatus("open") 
        } else {
            setStatus("")
        }
    }

    return (
        <header>
            <div className={`menu-icon ${status}`} onClick={openMenu}>
                <span></span>
                <span></span>
                <span></span>
            </div>  
            <nav className={status}>
                <Button label="início" path="/" classes={home && "selected"}/>
                <Button label="cotação das moedas" path="/moedas" classes={coins && "selected"}/>
            </nav>
        </header>
    )
}

export default Menu
