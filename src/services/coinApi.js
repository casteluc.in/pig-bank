import axios from 'axios'

const api = axios.create({
    baseURL: "https://economia.awesomeapi.com.br/"
})

function get_coin(setCards) {
    api.get("/json/all")
        .then( res => {
            let coinList = []
            Object.keys(res.data).forEach( key => {
                coinList.push(res.data[key])
            })
            setCards(coinList)
        }).catch( err => {
            console.log(err)
        })
}

export {get_coin}