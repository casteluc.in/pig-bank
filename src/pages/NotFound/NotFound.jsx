import React from 'react'

import Button from '../../common/Button/Button'
import Menu from '../../common/Header/Header'
import Footer from '../../common/Footer/Footer'
import {ReactComponent as Image} from '../../assets/not-found.svg'

import './NotFound.css'

function NotFound() {
    return (
        <>
        <div className="wrapper">
            <Menu/>
            <main>
                <section id="not-found">
                    <h2>Erro 404</h2>
                    <Image/>
                    <p>Ops! Parece que o endereço que você digitou está incorreto...</p>
                    <Button label="voltar" path="/"/>
                </section>
            </main>
            <Footer/>
        </div>
        </>
    )
}

export default NotFound 
