import React, { useEffect, useState} from 'react'

import Footer from '../../common/Footer/Footer'
import Menu from '../../common/Header/Header'
import CoinCard from './components/CoinCard'
import {ReactComponent as Image} from '../../assets/not-found.svg'
import { get_coin } from '../../services/coinApi'

import './Coins.css'

function Coins() {
    const [cards, setCards] = useState([])
    
    useEffect(() => {
        get_coin(setCards)
    }, [])

    return (
        <>
        <Menu coins/>
        <main>
            <section id="coins">
                <div className="container">
                    <h2>Cotação das Moedas</h2> 
                    <div className="cards">
                        {   
                            cards.map( card => {
                                return <CoinCard key={card.name} cardInfo={card}/>
                            })
                        }
                    </div>
                </div>
                <Image/>
                <div className="gradient"></div>
            </section>
        </main>
        <Footer fixed/>
        </>
    )
}

export default Coins
