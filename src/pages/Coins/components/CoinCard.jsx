import React from 'react'
import moment from 'moment'

moment.locale('pt-br')

function CoinCard({cardInfo}) {
    return (
        <div className="card">
            <div className="title">
                <h4>{cardInfo.name}</h4>
                <h3>{cardInfo.code}</h3>
            </div>
            <div className="price">
                <p>Máxima: R$ {cardInfo.high}</p>
                <p>Mínima: R$ {cardInfo.low}</p>
                <p>Atualizado em {moment(cardInfo.create_date).format('DD-MM-YYYY, hh:mm:ss ')}</p>
            </div>
        </div>
    )
}

export default CoinCard
