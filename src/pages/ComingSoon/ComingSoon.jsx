import React from 'react'

import Button from '../../common/Button/Button'
import Menu from '../../common/Header/Header'
import Footer from '../../common/Footer/Footer'
import {ReactComponent as Image} from '../../assets/soon.svg'

import './ComingSoon.css'

function ComingSoon() {
    return (
        <div className="wrapper">
        <Menu/>
        <main>
            <section id="coming-soon">
                <h2>Em construção</h2>
                <Image/>
                <p>
                    Parece que essa página ainda não foi implementada... <br/> 
                    Tente novamente mais tarde!
                </p>
                <Button label="voltar" path="/"/>
            </section>
        </main>
        <Footer/>
        </div>
    )
}

export default ComingSoon
