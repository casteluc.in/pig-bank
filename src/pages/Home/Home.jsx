import React from 'react'

import Menu from '../../common/Header/Header'
import Footer from '../../common/Footer/Footer'
import About from './components/About'
import HowTo from './components/HowTo'
import Goals from './components/Goals'

import './Home.css'

function Home() {
    return (
        <>
        <Menu home/>
        <main>
            <About/>
            <HowTo/>
            <Goals/>
        </main>
        <Footer/>
        </>
    )
}

export default Home
 