import React from 'react'
import {ReactComponent as Image1} from '../../../assets/how-to-1.svg'
import {ReactComponent as Image2} from '../../../assets/how-to-2.svg'
import {ReactComponent as Image3} from '../../../assets/how-to-3.svg'

import './HowToCard.css'

function HowToCard({title, cardNumber}) {
    return (
        <div className="card">
            {cardNumber == 1 && <Image1/>}
            {cardNumber == 2 && <Image2/>}
            {cardNumber == 3 && <Image3/>}
            <h3>{title}</h3>
            <p>Pellentesque faucibus leo sed arcu vulputate rhoncus. Integer sit amet blandit felis.</p>
        </div>
    )
}

export default HowToCard
