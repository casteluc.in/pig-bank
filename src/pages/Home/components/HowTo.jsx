import React from 'react'
import HowToCard from './HowToCard'

import './HowTo.css'

function HowTo() {
    return (
        <section id="how-to">
            <div className="container">
                <h2>Como usar</h2>

                <div className="cards">
                    <HowToCard title="defina uma meta" cardNumber="1"/>
                    <HowToCard title="acompanhe o progresso" cardNumber="2"/>
                    <HowToCard title="defina uma meta" cardNumber="3"/>
                </div>
            </div>
        </section>
    )
}

export default HowTo
