import React from 'react'
import {ReactComponent as Image} from '../../../assets/goals.svg'
import Button from '../../../common/Button/Button'

import './Goals.css'

function Goals() {
    return (
        <section id="goals" className="container">
            <div>
                <div>
                    <h2>Já tem uma meta?</h2>
                    <p>Phasellus mollis eget enim sed malesuada. Nulla facilisi. Aliquam nunc lacus, commodo hendrerit commodo pulvinar, suscipit eget ipsum. Nulla at sem ex.</p>
                    <Button label="ver metas" path="/em-breve"/>
                </div>
                <Image/>
            </div>
            <div>
                <h2>Não tem? Comece agora mesmo!</h2>
                <Button label="nova meta" path="/em-breve"/>
            </div>            
        </section>
    )
}

export default Goals
