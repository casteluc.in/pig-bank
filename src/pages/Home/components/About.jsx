import React from 'react'

import Button from '../../../common/Button/Button'
import {ReactComponent as Pig} from '../../../assets/pig-standing.svg'

import './About.css'

function About() {
    return (
        <section id="about" className="container">
            <div>
                <h1>Ping Bank</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a leo eu enim auctor faucibus id eget dolor. Pellentesque faucibus leo sed arcu vulputate rhoncus. </p>
                <Button label="nova meta" path="/em-breve"/>
            </div>
            <Pig/>
        </section>
    )
}

export default About
