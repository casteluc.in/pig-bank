import React from 'react'
import {BrowserRouter, Switch, Route} from 'react-router-dom'

import Home from './pages/Home/Home'
import ComingSoon from './pages/ComingSoon/ComingSoon'
import Coins from './pages/Coins/Coins'
import NotFound from './pages/NotFound/NotFound'

function Routes() {
    return(
        <BrowserRouter>
            <Switch>
                <Route exact path="/">
                    <Home/>
                </Route>

                <Route exact path="/em-breve">
                    <ComingSoon/>
                </Route>

                <Route exact path="/moedas">
                    <Coins/>
                </Route>

                <Route path="/">
                    <NotFound/>
                </Route>
            </Switch>
        </BrowserRouter>
    )
}

export default Routes